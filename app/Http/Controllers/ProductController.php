<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $products = Product::all();

        return response()->json(['data'=>$products],200);
    }


    public function store(Request $request)
    {
        $validar = Validator::make($request->all(),[
            'name'=>'required',
            'description'=>'required',
            'quantity'=>'required',
        ]);

        if($validar->fails()){
            $errors = $validar->errors();
            return response()->json(['errors'=>$errors],409);
        }

        $campos = $request->all();
        $campos['imagen'] = "ejemplo.png";

        $product = Product::create($campos);

        return response()->json(['data'=>$product],201);


    }

    public function show(Product $product)
    {
        return response()->json(['data'=>$product],200);
    }


    public function update(Request $request, Product $product)
    {

        if($request->name){
            $product->name = $request->name;
        }
        if($request->description){
            $product->description = $request->description;
        }
        if($request->quantity){
            $product->quantity = $request->quantity;
        }

        if(!$product->isDirty()){
            return response()->json(['error'=>'Se debe tener minimo un valor diferente.'],200);
        }

        $product->save();

        return response()->json(['data'=>$product],201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return response()->json(['data'=>$product],200);
    }
}
